from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests, json


def get_photos(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": city + " " + state, "per_page": 1}
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    params = {
        "appid": OPEN_WEATHER_API_KEY,
        "q": f"{city},{state},US",
        "limit": 1,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = json.loads(response.content)
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except (KeyError, IndexError):
        return {"lat_lon": None}

    weather_params = {
        "appid": OPEN_WEATHER_API_KEY,
        "lat": lat,
        "lon": lon,
        "units": "imperial",
    }
    weather_url = "https://api.openweathermap.org/data/2.5/weather?"
    weather_response = requests.get(weather_url, params=weather_params)
    weather_content = json.loads(weather_response.content)
    try:
        temp = weather_content["main"]["temp"]
        description = weather_content["weather"][0]["description"]
        return {"temp": temp, "description": description}
    except (KeyError, IndexError):
        return {"temp_description": None}
